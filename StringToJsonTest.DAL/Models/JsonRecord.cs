﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace StringToJsonTest.DAL.Models
{
    public class JsonRecord
    {
        public JsonRecord()
        {
        }

        public JsonRecord(int id, JObject json)
        {
            ID = id;
            JSON = json;
        }

        public int ID { get; set; }

        public JObject JSON { get; set; }
    }
}
