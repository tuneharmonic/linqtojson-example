﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StringToJsonTest.DAL.Models
{
    public class JsonRecordPrimitive
    {
        public int ID { get; set; }
        public string JSON { get; set; }
    }
}
