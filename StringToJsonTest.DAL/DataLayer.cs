﻿using Dapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Npgsql;
using StringToJsonTest.DAL.Models;
using System;
using System.Linq;

namespace StringToJsonTest.DAL
{
    public class DataLayer
    {
        private static string password = "****";    // Be sure to change this ;-)

        public static int AddJsonRecord(JObject json)
        {
            int id = 0;

            string connString = $"Host=localhost;Port=5432;Username=postgres;Password={password};Database=TestJsonDB;SSL Mode=Prefer";
            using (NpgsqlConnection conn = new NpgsqlConnection(connString))
            {
                string cmdText = "INSERT INTO public.\"JsonRecord\" (\"JSON\") VALUES (CAST(@JSON AS json)) RETURNING \"ID\"";
                object parameter = new { JSON = json.ToString(Formatting.None) };
                id = (int)conn.ExecuteScalar(cmdText, parameter);
            }

            return id;
        }

        public static JsonRecord GetJsonRecord(int id)
        {
            JsonRecord record = null;

            string connString = $"Host=localhost;Port=5432;Username=postgres;Password={password};Database=TestJsonDB;SSL Mode=Prefer";
            using (NpgsqlConnection conn = new NpgsqlConnection(connString))
            {
                string cmdText = "SELECT * FROM public.\"JsonRecord\" WHERE \"ID\" = @ID";
                object parameter = new { ID = id };
                JsonRecordPrimitive primitive = conn.Query<JsonRecordPrimitive>(cmdText, parameter).SingleOrDefault();
                if (primitive != null)
                {
                    record = new JsonRecord(primitive.ID, JObject.Parse(primitive.JSON));
                }
            }

            return record;
        }
    }
}
