﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using StringToJsonTest.DAL;
using StringToJsonTest.DAL.Models;
using System;

namespace StringToJsonTest.Test
{
    [TestFixture]
    public class JsonTests
    {
        [Test]
        public void FirstTest()
        {
            string json = "{\"topics\":{\"appearances\":{\"endTime\":\"[startTime]+7\",\"endSeconds\":\"[startSeconds]+7\"}}}";

            JObject jsonObject = JObject.Parse(json);

            Assert.AreEqual(jsonObject["topics"]["appearances"]["endTime"].ToString(), "[startTime]+7");

            string backToJson = jsonObject.ToString(Formatting.None);

            Assert.AreEqual(json, backToJson);
        }

        [Test]
        public void CanInsertJObject()
        {
            string json = "{\"topics\":{\"appearances\":{\"endTime\":\"[startTime]+7\",\"endSeconds\":\"[startSeconds]+7\"}}}";
            JObject jsonObject = JObject.Parse(json);

            int id = DataLayer.AddJsonRecord(jsonObject);

            Assert.That(id > 0);
        }

        [Test]
        public void CanGetJsonRecord()
        {
            JsonRecord record = DataLayer.GetJsonRecord(1);

            Assert.NotNull(record);
        }
    }
}
